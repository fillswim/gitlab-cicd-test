# GitLab-CICD-Test

## GitLab Runner в Docker
#### Установка:
[Install Runner](https://docs.gitlab.com/runner/install/docker.html)

```bash
docker run -d --name gitlab-runner --restart always \ 
	-v /srv/gitlab-runner/config:/etc/gitlab-runner \ 
	-v /var/run/docker.sock:/var/run/docker.sock \ 
	gitlab/gitlab-runner:latest
```
#### Регистрация:
[Register Runner](https://docs.gitlab.com/runner/register/)
```bash
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.com/" \
  --token "$RUNNER_TOKEN" \
  --description "docker-runner"
```

## GitLab Runner на Ubuntu

#### Установка:
```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

```bash
sudo apt-get install gitlab-runner
```
#### Регистрация:
```bash
gitlab-runner register --url https://gitlab.com --token XXXXXXXXXXXXXXXXXXXX
```

Установка GifService с помощью Helm (без Gitlab CI/CD)
```bash
helm install \
	my-gifservice \
	fillswimrepo/my-template-helmchart --namespace example-1 \
	--set container.image=fillswim/mygifservice-githubactions:latest \
	--set containerPort=8080 \
	--set servicePort=8080 \
	--set cpuUtilization=50 \
	--set memoryUtilization=80 \
	--set host=gifservice.fillswim.com
```
Просмотр установленных Helm в namespace example-1
```bash
helm list --namespace example-1
```
Удаление GifService c использованием Helm Chart
```bash
helm uninstall my-gifservice --namespace example-1
```
